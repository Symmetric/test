package nl.saeed.abn_amro_assessment.domain.repository

import io.reactivex.Single
import nl.saeed.abn_amro_assessment.domain.model.Venue
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails

interface VenueRepository {
    fun search(query: String): Single<List<Venue>>
    fun details(venueId: String): Single<VenueDetails>
}

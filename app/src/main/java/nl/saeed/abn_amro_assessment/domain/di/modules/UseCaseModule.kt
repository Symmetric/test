package nl.saeed.abn_amro_assessment.domain.di.modules

import dagger.Binds
import dagger.Module
import nl.saeed.abn_amro_assessment.domain.model.Venue
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails
import nl.saeed.abn_amro_assessment.domain.usecases.GetVenueDetails
import nl.saeed.abn_amro_assessment.domain.usecases.SearchVenues
import nl.saeed.abn_amro_assessment.domain.usecases.UseCaseSingleObserver

@Module
abstract class UseCaseModule {
    @Binds
    abstract fun provideSearchVenuesUseCase(searchVenues: SearchVenues): UseCaseSingleObserver<String, List<Venue>>

    @Binds
    abstract fun provideGetVenueDetailsUseCase(getVenueDetails: GetVenueDetails): UseCaseSingleObserver<String, VenueDetails>
}

package nl.saeed.abn_amro_assessment.domain.model

data class Location(val address: String, val city: String, val state: String) {
    override fun toString(): String {
        return "${address},${city},${state}"
    }
}

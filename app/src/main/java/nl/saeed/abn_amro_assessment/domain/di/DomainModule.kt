package nl.saeed.abn_amro_assessment.domain.di

import nl.saeed.abn_amro_assessment.domain.di.modules.UseCaseModule
import dagger.Module

@Module(includes = [UseCaseModule::class])
class DomainModule

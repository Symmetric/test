package nl.saeed.abn_amro_assessment.domain.model

data class Contact(
    val formattedPhone: String
)

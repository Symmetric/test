package nl.saeed.abn_amro_assessment.domain.usecases

import io.reactivex.Single

interface UseCaseSingleObserver<Input, Output> {
    fun execute(input: Input): Single<Output>
}

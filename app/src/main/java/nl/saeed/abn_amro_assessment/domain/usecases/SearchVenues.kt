package nl.saeed.abn_amro_assessment.domain.usecases

import io.reactivex.Single
import nl.saeed.abn_amro_assessment.data.repository.VenueRepository
import nl.saeed.abn_amro_assessment.domain.model.Venue
import javax.inject.Inject

class SearchVenues @Inject constructor(
    private val venuesRepository: VenueRepository
) : UseCaseSingleObserver<String, List<@JvmSuppressWildcards Venue>> {

    override fun execute(query: String): Single<List<Venue>> {
        return venuesRepository.search(query)
    }
}

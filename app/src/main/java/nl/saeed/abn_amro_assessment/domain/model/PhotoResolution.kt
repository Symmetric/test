package nl.saeed.abn_amro_assessment.domain.model

enum class PhotoResolution(val size: String) {
    THUMBNAIL("36x36"),
    LOW("100x100"),
    NORMAL("300x300"),
    HIGH("500x500"),
}

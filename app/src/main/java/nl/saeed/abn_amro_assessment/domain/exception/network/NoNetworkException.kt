package nl.saeed.abn_amro_assessment.domain.exception.network

import java.lang.Exception

class NoNetworkException : Exception()

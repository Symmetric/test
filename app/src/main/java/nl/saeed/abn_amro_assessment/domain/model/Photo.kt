package nl.saeed.abn_amro_assessment.domain.model

data class Photo(val id: String, private val prefix: String, private val suffix: String) {
    fun getUri(photoResolution: PhotoResolution) = prefix + photoResolution.size + suffix
}

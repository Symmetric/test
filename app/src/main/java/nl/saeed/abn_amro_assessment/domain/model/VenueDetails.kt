package nl.saeed.abn_amro_assessment.domain.model


data class VenueDetails(
    val id: String,
    val name: String,
    val location: Location,
    val photos: List<Photo>,
    val rating: Float,
    val description: String,
    val contact: Contact
)

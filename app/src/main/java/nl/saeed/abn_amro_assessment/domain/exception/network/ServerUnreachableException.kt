package nl.saeed.abn_amro_assessment.domain.exception.network

class ServerUnreachableException : Exception()

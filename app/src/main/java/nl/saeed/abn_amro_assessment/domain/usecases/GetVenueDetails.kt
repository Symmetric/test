package nl.saeed.abn_amro_assessment.domain.usecases

import io.reactivex.Single
import nl.saeed.abn_amro_assessment.data.repository.VenueRepository
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails
import javax.inject.Inject

class GetVenueDetails @Inject constructor(
    private val venuesRepository: VenueRepository
) : UseCaseSingleObserver<String, VenueDetails> {

    override fun execute(venueId: String): Single<VenueDetails> {
        return venuesRepository.details(venueId)
    }
}

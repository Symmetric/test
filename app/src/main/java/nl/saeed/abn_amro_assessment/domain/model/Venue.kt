package nl.saeed.abn_amro_assessment.domain.model

data class Venue(val id: String, val name: String, val location: Location)

package nl.saeed.abn_amro_assessment.app.di.modules

import android.content.Context
import nl.saeed.abn_amro_assessment.app.AbnAmroApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: AbnAmroApp): Context = application
}

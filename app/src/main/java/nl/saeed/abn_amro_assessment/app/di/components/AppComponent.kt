package nl.saeed.abn_amro_assessment.app.di.components

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import nl.saeed.abn_amro_assessment.app.AbnAmroApp
import nl.saeed.abn_amro_assessment.app.di.modules.AppModule
import nl.saeed.abn_amro_assessment.data.di.DataModule
import nl.saeed.abn_amro_assessment.domain.di.DomainModule
import nl.saeed.abn_amro_assessment.presentation.di.PresentationModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        DataModule::class,
        PresentationModule::class,
        DomainModule::class
    ]
)
interface AppComponent : AndroidInjector<AbnAmroApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: AbnAmroApp): Builder

        fun build(): AppComponent
    }

    override fun inject(app: AbnAmroApp)
}

package nl.saeed.abn_amro_assessment.presentation.ui.venue.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails
import nl.saeed.abn_amro_assessment.domain.usecases.UseCaseSingleObserver
import nl.saeed.abn_amro_assessment.presentation.ui.base.BaseViewModel
import nl.saeed.abn_amro_assessment.presentation.utils.ResultState
import javax.inject.Inject

class VenueDetailsFragmentViewModel @Inject constructor(
    private val searchVenuesUseCase: UseCaseSingleObserver<String, VenueDetails>
) : BaseViewModel() {

    private val venueResultStateMutable = MutableLiveData<ResultState<VenueDetails>>()
    val venueResultState: LiveData<ResultState<VenueDetails>> = venueResultStateMutable

    fun getVenueDetails(query: String) {
        disposables += searchVenuesUseCase.execute(query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { venueResultStateMutable.value = ResultState.Loading }
            .subscribe({
                venueResultStateMutable.value = ResultState.Result(it)
            }, {
                venueResultStateMutable.value = ResultState.Error(it)
            })
    }
}

package nl.saeed.abn_amro_assessment.presentation.ui.base

import android.content.Context
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment(@LayoutRes val layoutRes: Int) : Fragment(layoutRes) {

    override fun onAttach(context: Context) {
        performDependencyInjection()
        super.onAttach(context)
    }

    private fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
    }
}

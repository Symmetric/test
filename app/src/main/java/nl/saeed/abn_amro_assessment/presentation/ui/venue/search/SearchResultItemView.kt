package nl.saeed.abn_amro_assessment.presentation.ui.venue.search

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import com.lukeneedham.flowerpotrecycler.adapter.RecyclerItemView
import kotlinx.android.synthetic.main.view_search_result_item.view.*
import nl.saeed.abn_amro_assessment.R
import nl.saeed.abn_amro_assessment.domain.model.Venue

class SearchResultItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), RecyclerItemView<Venue> {

    init {
        View.inflate(context, R.layout.view_search_result_item, this)
        this.layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }

    override fun setItem(position: Int, item: Venue) {
        name.text = item.name
        location.text = item.location.toString()
    }
}

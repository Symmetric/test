package nl.saeed.abn_amro_assessment.presentation.di

import nl.saeed.abn_amro_assessment.presentation.di.builders.ActivityBuilderModule
import nl.saeed.abn_amro_assessment.presentation.di.builders.FragmentBuilderModule
import nl.saeed.abn_amro_assessment.presentation.di.modules.ViewModelModule
import dagger.Module
import nl.saeed.abn_amro_assessment.data.di.modules.MapperModule

@Module(
    includes = [
        ViewModelModule::class,
        ActivityBuilderModule::class,
        FragmentBuilderModule::class,
        MapperModule::class
    ]
)
class PresentationModule

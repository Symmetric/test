package nl.saeed.abn_amro_assessment.presentation.di.builders

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nl.saeed.abn_amro_assessment.presentation.di.modules.SearchFragmentModule
import nl.saeed.abn_amro_assessment.presentation.di.modules.VenueDetailsFragmentModule
import nl.saeed.abn_amro_assessment.presentation.ui.venue.details.VenueDetailsFragment
import nl.saeed.abn_amro_assessment.presentation.ui.venue.search.SearchFragment

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector(modules = [SearchFragmentModule::class])
    abstract fun contributeSearchFragment(): SearchFragment

    @ContributesAndroidInjector(modules = [VenueDetailsFragmentModule::class])
    abstract fun contributeVenueDetailsFragment(): VenueDetailsFragment
}

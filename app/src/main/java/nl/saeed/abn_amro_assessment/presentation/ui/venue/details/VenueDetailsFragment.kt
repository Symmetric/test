package nl.saeed.abn_amro_assessment.presentation.ui.venue.details

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_venue_details.*
import nl.saeed.abn_amro_assessment.R
import nl.saeed.abn_amro_assessment.domain.model.PhotoResolution
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails
import nl.saeed.abn_amro_assessment.presentation.di.ViewModelProviderFactory
import nl.saeed.abn_amro_assessment.presentation.ui.base.BaseFragment
import nl.saeed.abn_amro_assessment.presentation.utils.ResultState
import javax.inject.Inject

class VenueDetailsFragment : BaseFragment(R.layout.fragment_venue_details) {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    private lateinit var searchViewModel: VenueDetailsFragmentViewModel
    private val args: VenueDetailsFragmentArgs by navArgs()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        createViewModel()
        observeViewModelLiveData()
    }

    private fun createViewModel() {
        searchViewModel = ViewModelProvider(this, viewModelProviderFactory)
            .get(VenueDetailsFragmentViewModel::class.java)

        searchViewModel.getVenueDetails(args.venueId)
    }

    private fun observeViewModelLiveData() {
        searchViewModel.venueResultState.observe(viewLifecycleOwner) {

            when (it) {
                ResultState.Loading ->
                    Toast.makeText(requireContext(), "loading", Toast.LENGTH_SHORT)
                        .show()
                is ResultState.Result -> populateData(it.result)
                is ResultState.Error -> Toast.makeText(
                    requireContext(),
                    "${it.error.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun populateData(venueDetails: VenueDetails) {
        if (!venueDetails.photos.isNullOrEmpty()) {
            Log.d("photo", venueDetails.photos.first().getUri(PhotoResolution.NORMAL))
            Glide.with(image.context)
                .load(venueDetails.photos.first().getUri(PhotoResolution.NORMAL))
                .into(image)
        }
        name.text = venueDetails.name
        description.text = venueDetails.description
        contactInformation.text = venueDetails.contact.formattedPhone
        address.text = venueDetails.location.toString()
        rating.text = venueDetails.rating.toString()
    }
}

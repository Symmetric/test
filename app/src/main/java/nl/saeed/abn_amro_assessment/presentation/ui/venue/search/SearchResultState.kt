package nl.saeed.abn_amro_assessment.presentation.ui.venue.search

import nl.saeed.abn_amro_assessment.domain.model.Venue

sealed class SearchResultState {
    object Loading : SearchResultState()
    object Empty : SearchResultState()
    class Result(val result: List<Venue>) : SearchResultState()
    class Error(val error: Throwable) : SearchResultState()
}

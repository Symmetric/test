package nl.saeed.abn_amro_assessment.presentation.di.builders

import dagger.Module
import dagger.android.ContributesAndroidInjector
import nl.saeed.abn_amro_assessment.presentation.ui.MainActivity

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun bindMainActivity(): MainActivity
}

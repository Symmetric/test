package nl.saeed.abn_amro_assessment.presentation.ui.venue.search

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.lukeneedham.flowerpotrecycler.SingleTypeRecyclerAdapterCreator
import com.lukeneedham.flowerpotrecycler.adapter.config.SingleTypeAdapterConfig
import com.lukeneedham.flowerpotrecycler.util.extensions.addOnItemClickListener
import kotlinx.android.synthetic.main.fragment_search.*
import nl.saeed.abn_amro_assessment.R
import nl.saeed.abn_amro_assessment.domain.model.Venue
import nl.saeed.abn_amro_assessment.presentation.di.ViewModelProviderFactory
import nl.saeed.abn_amro_assessment.presentation.ui.base.BaseFragment
import javax.inject.Inject

class SearchFragment : BaseFragment(R.layout.fragment_search) {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    private lateinit var searchViewModel: SearchFragmentViewModel

    private val venuesAdapter =
        SingleTypeRecyclerAdapterCreator.fromRecyclerItemView(
            SingleTypeAdapterConfig<Venue, SearchResultItemView>().apply {
                addOnItemClickListener { item, _, _ -> navigateToVenueDetails(item) }
            }

        )

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        createViewModel()
        observeViewModelLiveData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchResultView()
        setupQueryInputView()
    }

    private fun setupSearchResultView() {
        searchResultList.layoutManager = LinearLayoutManager(requireContext())
        searchResultList.adapter = venuesAdapter
    }

    private fun setupQueryInputView() {
        queryInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchViewModel.search(queryInput.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun createViewModel() {
        searchViewModel = ViewModelProvider(this, viewModelProviderFactory)
            .get(SearchFragmentViewModel::class.java)
    }

    private fun observeViewModelLiveData() {
        searchViewModel.searchResult.observe(viewLifecycleOwner) {
            searchResultList.visibility = if (it is SearchResultState.Result) {
                loadingIndicator.visibility = View.INVISIBLE
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
            when (it) {
                SearchResultState.Loading -> loadingIndicator.visibility = View.VISIBLE
                SearchResultState.Empty ->
                    Toast.makeText(requireContext(), "no result found", Toast.LENGTH_LONG)
                        .show()
                is SearchResultState.Result -> venuesAdapter.submitList(it.result)
                is SearchResultState.Error ->
                    Toast.makeText(requireContext(), "something went wrong", Toast.LENGTH_LONG)
                        .show()
            }
        }
    }

    private fun navigateToVenueDetails(venue: Venue) {
        findNavController().navigate(
            SearchFragmentDirections.actionSearchFragmentToVenueDetailsFragment(venue.id)
        )
    }
}

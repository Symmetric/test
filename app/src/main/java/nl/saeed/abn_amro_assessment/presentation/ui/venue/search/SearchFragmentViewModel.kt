package nl.saeed.abn_amro_assessment.presentation.ui.venue.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import nl.saeed.abn_amro_assessment.domain.model.Venue
import nl.saeed.abn_amro_assessment.domain.usecases.UseCaseSingleObserver
import nl.saeed.abn_amro_assessment.presentation.ui.base.BaseViewModel
import javax.inject.Inject

class SearchFragmentViewModel @Inject constructor(
    private val searchVenuesUseCase: UseCaseSingleObserver<String, List<Venue>>
) : BaseViewModel() {

    private val searchResultMutable = MutableLiveData<SearchResultState>()
    val searchResult: LiveData<SearchResultState> = searchResultMutable

    fun search(query: String) {
        disposables += searchVenuesUseCase.execute(query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { searchResultMutable.value = SearchResultState.Loading }
            .subscribe({
                searchResultMutable.value = if (it.isNullOrEmpty()) {
                    SearchResultState.Empty
                } else {
                    SearchResultState.Result(it)
                }
            }, {
                searchResultMutable.value = SearchResultState.Error(it)
            })
    }
}

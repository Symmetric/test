package nl.saeed.abn_amro_assessment.presentation.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bol.assessment.presentation.di.interfaces.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import nl.saeed.abn_amro_assessment.presentation.di.ViewModelProviderFactory
import nl.saeed.abn_amro_assessment.presentation.ui.venue.details.VenueDetailsFragmentViewModel
import nl.saeed.abn_amro_assessment.presentation.ui.venue.search.SearchFragmentViewModel

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SearchFragmentViewModel::class)
    abstract fun bindSearchFragmentViewModel(viewModel: SearchFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VenueDetailsFragmentViewModel::class)
    abstract fun bindVenueDetailsFragmentViewModel(viewModel: VenueDetailsFragmentViewModel): ViewModel
}

package nl.saeed.abn_amro_assessment.presentation.utils

sealed class ResultState<out T> {
    object Loading : ResultState<Nothing>()
    class Result<T>(val result: T) : ResultState<T>()
    class Error(val error: Throwable) : ResultState<Nothing>()
}

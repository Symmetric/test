package nl.saeed.abn_amro_assessment.data.api.model

data class LatitudeLongitude(val latitude: Double, val longitude: Double) {
    override fun toString(): String {
        return "$latitude,$longitude"
    }
}

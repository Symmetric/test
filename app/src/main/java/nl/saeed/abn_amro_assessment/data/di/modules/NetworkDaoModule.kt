package nl.saeed.abn_amro_assessment.data.di.modules

import dagger.Binds
import dagger.Module
import nl.saeed.abn_amro_assessment.data.api.dao.VenuesDao
import nl.saeed.abn_amro_assessment.data.api.dao.inferstructure.VenuesApiNetDao

@Module
abstract class NetworkDaoModule {
    @Binds
    abstract fun provideVenueDao(venueDao: VenuesApiNetDao): VenuesDao
}

package nl.saeed.abn_amro_assessment.data.api.model.responses.venue

data class VenueData(val id: String, val name: String, val location: Location)

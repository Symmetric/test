package nl.saeed.abn_amro_assessment.data.di.modules

import nl.saeed.abn_amro_assessment.domain.repository.VenueRepository as VenueRepositoryInterface
import dagger.Binds
import dagger.Module
import nl.saeed.abn_amro_assessment.data.repository.VenueRepository

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun provideVenueRepository(venueRepository: VenueRepository): VenueRepositoryInterface
}

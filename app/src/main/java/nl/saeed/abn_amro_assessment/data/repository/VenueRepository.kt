package nl.saeed.abn_amro_assessment.data.repository

import io.reactivex.functions.Function
import nl.saeed.abn_amro_assessment.data.api.dao.VenuesDao
import nl.saeed.abn_amro_assessment.data.api.model.LatitudeLongitude
import nl.saeed.abn_amro_assessment.data.utils.extensions.mapNetworkErrors
import javax.inject.Inject
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.VenueData as VenueApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.VenueDetailsData as VenueDetailsApi
import nl.saeed.abn_amro_assessment.domain.model.Venue as VenueDomain
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails as VenueDetailsDomain
import nl.saeed.abn_amro_assessment.domain.repository.VenueRepository as VenueRepositoryInterface

class VenueRepository @Inject constructor(
    private val venuesDao: VenuesDao,
    private val venueDetailsMapper: Function<VenueDetailsApi, VenueDetailsDomain>,
    private val venuesMapper: Function<VenueApi, VenueDomain>
) : VenueRepositoryInterface {
    override fun search(query: String) =
        venuesDao.search(query, SEARCH_RADIUS, SEARCH_LATITUDE_LONGITUDE.toString(), SEARCH_LIMIT)
            .mapNetworkErrors()
            .map { it.response.venues }
            .map { venues -> venues.map { venue -> venuesMapper.apply(venue) } }

    override fun details(venueId: String) = venuesDao.details(venueId)
        .mapNetworkErrors()
        .map { it.response.venue }
        .map(venueDetailsMapper)

    companion object {
        const val SEARCH_RADIUS = 1000
        const val SEARCH_LIMIT = 10

        // Rotterdam Lat & Long
        val SEARCH_LATITUDE_LONGITUDE = LatitudeLongitude(51.9244201, 4.4777326)
    }
}

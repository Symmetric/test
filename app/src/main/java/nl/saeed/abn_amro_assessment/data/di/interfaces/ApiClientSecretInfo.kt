package nl.saeed.abn_amro_assessment.data.di.interfaces

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiClientSecretInfo

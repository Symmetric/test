package nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details

data class VenueDetails(val venue: VenueDetailsData)

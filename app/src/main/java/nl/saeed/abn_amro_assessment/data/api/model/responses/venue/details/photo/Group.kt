package nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo

data class Group(val type: String, val name: String, val count: Int, val items: List<PhotoItem>)

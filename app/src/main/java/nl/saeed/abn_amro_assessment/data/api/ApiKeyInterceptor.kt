package nl.saeed.abn_amro_assessment.data.api

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ApiKeyInterceptor @Inject constructor(
    private val clientId: String,
    private val clientSecret: String,
    private val apiVersion: String
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val url = request.url.newBuilder()
            .addQueryParameter("client_id", clientId)
            .addQueryParameter("client_secret", clientSecret)
            .addQueryParameter("v", apiVersion)
            .build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}

package nl.saeed.abn_amro_assessment.data.di.modules

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ParserModule {

    @Provides
    @Singleton
    fun getMoshi() = baseMoshiBuilder.add(KotlinJsonAdapterFactory()).build()

    private val baseMoshiBuilder = Moshi.Builder()
}

package nl.saeed.abn_amro_assessment.data.api.dao

import io.reactivex.Single
import nl.saeed.abn_amro_assessment.data.api.model.responses.Response
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Venues
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.VenueDetails

interface VenuesDao {

    /**
     * @param query term to search for
     * @param radius the radius in which to search for the given term around the specified [latitudeLongitude]
     * @param latitudeLongitude the latitude and longitude of location to search for term in
     * @param limit number of search result to return
     */
    fun search(query: String, radius: Int, latitudeLongitude: String, limit: Int): Single<Response<Venues>>

    fun details(venueId: String): Single<Response<VenueDetails>>
}

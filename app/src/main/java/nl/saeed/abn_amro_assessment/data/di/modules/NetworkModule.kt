package nl.saeed.abn_amro_assessment.data.di.modules

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import nl.saeed.abn_amro_assessment.BuildConfig
import nl.saeed.abn_amro_assessment.data.api.ApiKeyInterceptor
import nl.saeed.abn_amro_assessment.data.api.ApiService
import nl.saeed.abn_amro_assessment.data.di.interfaces.ApiClientIdInfo
import nl.saeed.abn_amro_assessment.data.di.interfaces.ApiClientSecretInfo
import nl.saeed.abn_amro_assessment.data.di.interfaces.ApiUrlInfo
import nl.saeed.abn_amro_assessment.data.di.interfaces.ApiVersionInfo
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @ApiUrlInfo
    fun provideApiUrlInfo(): String = BuildConfig.REST_API_BASE_URL

    @Provides
    @ApiClientIdInfo
    fun provideApiClientId(): String = BuildConfig.API_CLIENT_ID

    @Provides
    @ApiClientSecretInfo
    fun provideApiClientSecret(): String = BuildConfig.API_CLIENT_SECRET

    @Provides
    @ApiVersionInfo
    fun provideApiVersion(): String = "20210101"

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        moshi: Moshi,
        @ApiUrlInfo baseUrl: String
    ): Retrofit =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    fun provideApiService(retrofit: Retrofit) =
        retrofit.create(ApiService::class.java)

    @Provides
    fun provideApiKeyInterceptor(
        @ApiClientIdInfo clientId: String,
        @ApiClientSecretInfo clientSecret: String,
        @ApiVersionInfo apiVersion: String,
    ) = ApiKeyInterceptor(clientId, clientSecret, apiVersion)

    @Provides
    fun provideHTTPLoggingInterceptor() =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        interceptor: Interceptor
    ) = OkHttpClient()
        .newBuilder()
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(interceptor)
        .readTimeout(20, TimeUnit.SECONDS)
        .connectTimeout(20, TimeUnit.SECONDS)
        .retryOnConnectionFailure(true)
        .build()
}

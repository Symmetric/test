package nl.saeed.abn_amro_assessment.data.api

import io.reactivex.Single
import nl.saeed.abn_amro_assessment.data.api.model.responses.Response
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Venues
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.VenueDetails
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("$API_VERSION/$VENUES_URL_PATH_KEY/search")
    fun searchVenues(
        @Query(QUERY_SEARCH_TERM) query: String,
        @Query(QUERY_RADIUS) radius: Int,
        @Query(QUERY_LATITUDE_LONGITUDE) latitudeLongitude: String,
        @Query(QUERY_LIMIT) limit: Int,
    ): Single<Response<Venues>>

    @GET("$API_VERSION/$VENUES_URL_PATH_KEY/{$PATH_VENUE_ID}")
    fun getVenueDetails(@Path(PATH_VENUE_ID) venueId: String): Single<Response<VenueDetails>>

    companion object {
        const val API_VERSION = "v2"
        const val VENUES_URL_PATH_KEY = "venues"

        const val QUERY_RADIUS = "radius"
        const val QUERY_LATITUDE_LONGITUDE = "ll"
        const val QUERY_LIMIT = "limit"
        const val QUERY_SEARCH_TERM = "query"

        const val PATH_VENUE_ID = "id"
    }
}

package nl.saeed.abn_amro_assessment.data.mappers

import io.reactivex.functions.Function
import javax.inject.Inject
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo.PhotoItem as PhotoApi
import nl.saeed.abn_amro_assessment.domain.model.Photo as PhotoDomain

class PhotoApiToDomainMapper @Inject constructor() : Function<PhotoApi, PhotoDomain> {
    override fun apply(t: PhotoApi) = PhotoDomain(t.id, t.prefix, t.suffix)
}

package nl.saeed.abn_amro_assessment.data.mappers

import io.reactivex.functions.Function
import javax.inject.Inject
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Location as LocationApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.VenueData as VenueApi
import nl.saeed.abn_amro_assessment.domain.model.Location as LocationDomain
import nl.saeed.abn_amro_assessment.domain.model.Venue as VenueDomain

class VenueApiToDomainMapper @Inject constructor(
    private val locationMapper: Function<LocationApi, LocationDomain>
) : Function<VenueApi, VenueDomain> {
    override fun apply(t: VenueApi) = VenueDomain(t.id, t.name, locationMapper.apply(t.location))
}

package nl.saeed.abn_amro_assessment.data.api.model.responses

data class Response<T>(val response: T)

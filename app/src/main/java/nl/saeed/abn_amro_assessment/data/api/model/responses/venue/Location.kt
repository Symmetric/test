package nl.saeed.abn_amro_assessment.data.api.model.responses.venue

data class Location(
    val address: String?,
    val city: String?,
    val state: String?
)

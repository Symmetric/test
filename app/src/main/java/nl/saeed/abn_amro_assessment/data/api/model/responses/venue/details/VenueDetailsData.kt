package nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details

import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Location
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo.PhotoData

data class VenueDetailsData(
    val id: String,
    val name: String,
    val location: Location,
    val photos: PhotoData,
    val rating: Float?,
    val description: String?,
    val contact: Contact
)

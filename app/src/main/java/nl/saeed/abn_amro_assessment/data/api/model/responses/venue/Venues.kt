package nl.saeed.abn_amro_assessment.data.api.model.responses.venue

data class Venues(val venues: List<VenueData>)

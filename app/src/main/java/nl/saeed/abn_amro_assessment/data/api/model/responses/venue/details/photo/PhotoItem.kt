package nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo

data class PhotoItem(
    val id: String,
    val prefix: String,
    val suffix: String,
    val width: Int,
    val height: Int
)

package nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo

data class PhotoData (val count: Int, val groups: List<Group>)

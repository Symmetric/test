package nl.saeed.abn_amro_assessment.data.di.modules

import dagger.Binds
import dagger.Module
import io.reactivex.functions.Function
import nl.saeed.abn_amro_assessment.data.mappers.ContactApiToDomainMapper
import nl.saeed.abn_amro_assessment.data.mappers.LocationApiToDomainMapper
import nl.saeed.abn_amro_assessment.data.mappers.PhotoApiToDomainMapper
import nl.saeed.abn_amro_assessment.data.mappers.VenueApiToDomainMapper
import nl.saeed.abn_amro_assessment.data.mappers.VenueDetailsApiToDomainMapper
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Location as LocationApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.VenueData as VenueApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.Contact as ContactApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.VenueDetailsData as VenueDetailsApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo.PhotoItem as PhotoApi
import nl.saeed.abn_amro_assessment.domain.model.Contact as ContactDomain
import nl.saeed.abn_amro_assessment.domain.model.Location as LocationDomain
import nl.saeed.abn_amro_assessment.domain.model.Photo as PhotoDomain
import nl.saeed.abn_amro_assessment.domain.model.Venue as VenueDomain
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails as VenueDetailsDomain

@Module
abstract class MapperModule {
    @Binds
    abstract fun providePhotoMapper(photoMapper: PhotoApiToDomainMapper): Function<PhotoApi, PhotoDomain>

    @Binds
    abstract fun provideLocationMapper(locationMapper: LocationApiToDomainMapper): Function<LocationApi, LocationDomain>

    @Binds
    abstract fun provideVenueMapper(venueMapper: VenueApiToDomainMapper): Function<VenueApi, VenueDomain>

    @Binds
    abstract fun provideVenueDetailsMapper(venueDetailsMapper: VenueDetailsApiToDomainMapper): Function<VenueDetailsApi, VenueDetailsDomain>

    @Binds
    abstract fun provideContactMapper(contactMapper: ContactApiToDomainMapper): Function<ContactApi, ContactDomain>
}

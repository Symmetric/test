package nl.saeed.abn_amro_assessment.data.utils.extensions

import io.reactivex.Single
import nl.saeed.abn_amro_assessment.domain.exception.network.NoNetworkException
import nl.saeed.abn_amro_assessment.domain.exception.network.NotFoundException
import nl.saeed.abn_amro_assessment.domain.exception.network.ServerUnreachableException
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import nl.saeed.abn_amro_assessment.domain.exception.network.HttpException as HttpExceptionDomain

internal fun <T> Single<T>.mapNetworkErrors(): Single<T> {
    return this.onErrorResumeNext { error ->
        when (error) {
            is SocketTimeoutException -> Single.error(NoNetworkException())
            is UnknownHostException -> Single.error(ServerUnreachableException())
            is ConnectException -> Single.error(NoNetworkException())
            is HttpException -> {
                if (error.code() == 404) {
                    Single.error(NotFoundException())
                } else {
                    Single.error(HttpExceptionDomain())
                }
            }
            else -> Single.error(error)
        }
    }
}

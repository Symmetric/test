package nl.saeed.abn_amro_assessment.data.di

import dagger.Module
import nl.saeed.abn_amro_assessment.data.di.modules.MapperModule
import nl.saeed.abn_amro_assessment.data.di.modules.NetworkBindingModule
import nl.saeed.abn_amro_assessment.data.di.modules.NetworkDaoModule
import nl.saeed.abn_amro_assessment.data.di.modules.NetworkModule
import nl.saeed.abn_amro_assessment.data.di.modules.ParserModule
import nl.saeed.abn_amro_assessment.data.di.modules.RepositoryModule

@Module(
    includes = [
        NetworkDaoModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        MapperModule::class,
        ParserModule::class,
        NetworkBindingModule::class
    ]
)
class DataModule

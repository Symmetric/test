package nl.saeed.abn_amro_assessment.data.mappers

import io.reactivex.functions.Function
import javax.inject.Inject
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Location as LocationApi
import nl.saeed.abn_amro_assessment.domain.model.Location as LocationDomain

class LocationApiToDomainMapper @Inject constructor() : Function<LocationApi, LocationDomain> {
    override fun apply(t: LocationApi) =
        LocationDomain(t.address ?: "", t.city ?: "", t.state ?: "")
}

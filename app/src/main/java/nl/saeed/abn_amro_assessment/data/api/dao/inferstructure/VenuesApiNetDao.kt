package nl.saeed.abn_amro_assessment.data.api.dao.inferstructure

import nl.saeed.abn_amro_assessment.data.api.ApiService
import nl.saeed.abn_amro_assessment.data.api.dao.VenuesDao
import javax.inject.Inject

class VenuesApiNetDao @Inject constructor(private val apiService: ApiService) : VenuesDao {
    override fun search(
        query: String,
        radius: Int,
        latitudeLongitude: String,
        limit: Int
    ) = apiService.searchVenues(query, radius, latitudeLongitude, limit)

    override fun details(venueId: String) = apiService.getVenueDetails(venueId)
}

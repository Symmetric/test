package nl.saeed.abn_amro_assessment.data.di.modules

import dagger.Binds
import dagger.Module
import nl.saeed.abn_amro_assessment.data.api.ApiKeyInterceptor
import okhttp3.Interceptor

@Module
abstract class NetworkBindingModule {
    @Binds
    abstract fun bindApiKeyInterceptor(apiKeyInterceptor: ApiKeyInterceptor): Interceptor
}

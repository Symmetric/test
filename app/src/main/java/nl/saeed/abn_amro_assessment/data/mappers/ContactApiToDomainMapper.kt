package nl.saeed.abn_amro_assessment.data.mappers

import io.reactivex.functions.Function
import javax.inject.Inject
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.Contact as ContactApi
import nl.saeed.abn_amro_assessment.domain.model.Contact as ContactDomain

class ContactApiToDomainMapper @Inject constructor() : Function<ContactApi, ContactDomain> {
    override fun apply(t: ContactApi) = ContactDomain(t.formattedPhone ?: "No formatted phone")
}

package nl.saeed.abn_amro_assessment.data.mappers

import io.reactivex.functions.Function
import javax.inject.Inject
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.Location as LocationApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.Contact as ContactApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.VenueDetailsData as VenueDetailsApi
import nl.saeed.abn_amro_assessment.data.api.model.responses.venue.details.photo.PhotoItem as PhotoApi
import nl.saeed.abn_amro_assessment.domain.model.Contact as ContactDomain
import nl.saeed.abn_amro_assessment.domain.model.Location as LocationDomain
import nl.saeed.abn_amro_assessment.domain.model.Photo as PhotoDomain
import nl.saeed.abn_amro_assessment.domain.model.VenueDetails as VenueDetailsDomain

class VenueDetailsApiToDomainMapper @Inject constructor(
    private val locationMapper: Function<LocationApi, LocationDomain>,
    private val photoMapper: Function<PhotoApi, PhotoDomain>,
    private val contactMapper: Function<ContactApi, ContactDomain>,
) : Function<VenueDetailsApi, VenueDetailsDomain> {
    override fun apply(t: VenueDetailsApi) =
        VenueDetailsDomain(
            t.id,
            t.name,
            locationMapper.apply(t.location),
            t.photos.groups.map { it.items }.flatten().map { photoMapper.apply(it) },
            t.rating ?: -1.0F,
            t.description ?: "No Description",
            contactMapper.apply(t.contact)
        )
}
